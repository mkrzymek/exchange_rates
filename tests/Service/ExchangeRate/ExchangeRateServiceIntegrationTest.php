<?php
/**
 * Created by PhpStorm.
 * User: michal
 * Date: 14.09.18
 * Time: 13:00
 */

namespace Tests\Service\ExchangeRate;


use App\Entity\Currency;
use App\Entity\ExchangeRateHistory;
use App\Repository\CurrencyRepository;
use App\Service\ExchangeRate\ExchangeRate;
use App\Service\ExchangeRate\ExchangeRateCollection;
use App\Service\ExchangeRate\ExchangeRateProvider;
use App\Service\ExchangeRate\ExchangeRateService;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class ExchangeRateServiceIntegrationTest extends KernelTestCase
{
    public function setUp()
    {
        self::bootKernel();

        $this->truncateEntities();
    }

    public function testAddExchangeRates()
    {
        /** @var ExchangeRateService $exchangeRateService */
//        $exchangeRateService = self::$kernel->getContainer()->get('test.' . ExchangeRateService::class);
        $currency = new Currency();
        $currency->setCode('USD')
            ->setCurrencyName('dolar amerykanski');

        $this->getEntityManager()->persist($currency);
        $this->getEntityManager()->flush();


        $repository = $this->createMock(CurrencyRepository::class);
        $repository->expects($this->any())
            ->method('getActiveCurrenciesWithLatestExchangeRate')
            ->willReturn([$currency]);

        $provider = $this->createMock(ExchangeRateProvider::class);
        $provider->expects($this->any())
            ->method('fetch')
            ->willReturn(new ExchangeRateCollection([
                'USD' => new ExchangeRate('USD', 'name', 1.23, new
                \DateTime('now'))
            ]));

        $exchangeRateService = new ExchangeRateService($provider, $this->getEntityManager(), $repository);

        $exchangeRateService->addExchangeRates();

        $em = $this->getEntityManager();
        $count = (int) $em->getRepository(ExchangeRateHistory::class)
            ->createQueryBuilder('erh')
            ->select('COUNT(erh.id)')
            ->getQuery()
            ->getSingleScalarResult();

        $this->assertSame(1, $count, 'Amount of Exchange Rates is not the same');
    }

    private function truncateEntities()
    {
        $purger = new ORMPurger($this->getEntityManager());
        $purger->purge();
    }

    /**
     * @return EntityManager
     */
    private function getEntityManager()
    {
        return self::$kernel->getContainer()
            ->get('doctrine')
            ->getManager();
    }
}