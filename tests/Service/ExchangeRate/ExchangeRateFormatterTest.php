<?php

namespace Tests\Service\ExchangeRate;


use App\Service\ExchangeRate\ExchangeRateCollection;
use App\Service\ExchangeRate\ExchangeRateFormatter;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Debug\Exception\UndefinedFunctionException;

class ExchangeRateFormatterTest extends TestCase
{
    /** @var  ExchangeRateFormatter */
    private $formatter;

    public function setUp()
    {
        $this->formatter = new ExchangeRateFormatter();
    }

    /**
     * @dataProvider ratesAndDateProvider
     * @param \dateTime $date
     * @param array $rates
     */
    public function testItReturnsCorrectCodeNameMidRateAndEffectiveDate(\dateTime $date,array $rates)
    {
        /** @var ExchangeRateCollection $exchangeRates */
        $exchangeRates = $this->formatter->ratesFormatterNBP($rates, $date);

        $this->assertInstanceOf(ExchangeRateCollection::class, $exchangeRates);
        $this->assertSame($date, $exchangeRates->offsetGet('USD')->getEffectiveDate());
        $this->assertSame(3.7198, $exchangeRates->offsetGet('USD')->getMidRate());
        $this->assertSame('USD', $exchangeRates->offsetGet('USD')->getCode());
        $this->assertSame('dolar amerykański', $exchangeRates->offsetGet('USD')->getName());
    }

    public function ratesAndDateProvider()
    {
        $date = new \DateTime('2018-09-13');

        $usd = new \stdClass();
        $usd->currency = 'dolar amerykański';
        $usd->code = 'USD';
        $usd->mid = 3.7198;

        return [
            'case 1' => [$date, [$usd]]
        ];
    }

}