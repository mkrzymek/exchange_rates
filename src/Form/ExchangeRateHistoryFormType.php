<?php

namespace App\Form;


use App\Repository\CurrencyRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;

class ExchangeRateHistoryFormType extends AbstractType
{
    /** @var CurrencyRepository */
    private $repository;

    public function __construct(CurrencyRepository $repository)
    {

        $this->repository = $repository;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $currencies = $this->repository->findBy(['isActive' => true],['code' => 'ASC']);
        $dropDown['All Currencies'] = '';

        foreach ($currencies as $currency) {
            $dropDown[$currency->getCode()] = $currency->getCode();
        }

        $builder
            ->setMethod('GET')
            ->add('currencies', ChoiceType::class, [
                'choices'  => $dropDown,
                'multiple' => true,
            ])
            ->add('fromDate', DateType::class, [
                'widget' => 'single_text',
                'html5' => false,
                'attr' => [
                    'class' => 'js-datepicker',
                ],
            ])
            ->add('toDate', DateType::class, [
                'widget' => 'single_text',
                'html5' => false,
                'attr' => [
                    'class' => 'js-datepicker',
                ],
            ]);
    }
}