<?php

namespace App\DataFixtures;

use App\Entity\Currency;
use App\Entity\ExchangeRateHistory;
use Doctrine\Common\Persistence\ObjectManager;

class ExchangeRateHistoryFixture extends BaseFixture
{
    public function loadData(ObjectManager $manager)
    {
        for ($i = 0; $i < count(CurrencyFixture::$codes); $i++) {
            for ($j = 1; $j < 40; $j++) {
                $exchangeRate = new ExchangeRateHistory();
                $exchangeRate->setRate($this->faker->randomFloat(5, 0, 4))
                    ->setCurrency($this->getReference(Currency::class . '_' . $i))
                    ->setEffectiveDate((new \DateTime('now'))->modify('-' . $j . ' day'));

                $manager->persist($exchangeRate);
            }
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            CurrencyFixture::class,
        ];
    }
}
