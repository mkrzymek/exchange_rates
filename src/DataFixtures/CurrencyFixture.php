<?php

namespace App\DataFixtures;

use App\Entity\Currency;
use App\Entity\ExchangeRateHistory;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class CurrencyFixture extends BaseFixture
{
    static $currencies = [
        'THB' => 'bat (Tajlandia)',
        'USD' => 'dolar amerykański',
        'HKD' => 'dolar Hongkongu',
        'CAD' => 'dolar kanadyjski',
        'NZD' => 'dolar nowozelandzki',
        'SGD' => 'dolar singapurski',
        'EUR' => 'euro',
        'GBP' => 'funt szterling',
        'CZK' => 'korona czeska',
        'DKK' => 'korona duńska',
        'ISK' => 'korona islandzka',
        'IDR' => 'rupia indonezyjska',
    ];

    static $codes = [
        'THB',
        'USD',
        'HKD',
        'CAD',
        'NZD',
        'SGD',
        'EUR',
        'GBP',
        'CZK',
        'DKK',
        'ISK',
        'IDR',
    ];

    public function loadData(ObjectManager $manager)
    {
        $this->createMany(Currency::class, count(self::$codes), function (Currency $currency, $count) use ($manager) {
            $currency->setCode(self::$codes[$count])
                ->setCurrencyName(self::$currencies[self::$codes[$count]]);
        });

        $manager->flush();
    }
}
