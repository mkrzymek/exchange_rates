<?php

namespace App\Command;

use App\Service\ExchangeRate\CurrencyService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class UpdateCurrenciesCommand extends Command
{
    protected static $defaultName = 'app:update:currencies';

    /** @var CurrencyService */
    private $service;

    public function __construct(CurrencyService $service)
    {
        parent::__construct();
        $this->service = $service;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);

        $this->service->addCurrencies();

        $io->success('Exchange rate updated! Dear developer it would be nice to create crontab (mon-fri 12:20) :D');
    }
}
