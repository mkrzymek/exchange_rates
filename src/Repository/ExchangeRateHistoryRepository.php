<?php

namespace App\Repository;

use App\Entity\ExchangeRateHistory;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bridge\Doctrine\RegistryInterface;
use function Symfony\Component\DependencyInjection\Loader\Configurator\expr;

/**
 * @method ExchangeRateHistory|null find($id, $lockMode = null, $lockVersion = null)
 * @method ExchangeRateHistory|null findOneBy(array $criteria, array $orderBy = null)
 * @method ExchangeRateHistory[]    findAll()
 * @method ExchangeRateHistory[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ExchangeRateHistoryRepository extends ServiceEntityRepository
{
    const ACTIVE = 1;

    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ExchangeRateHistory::class);
    }

    public function getLatestActiveExchangeRates()
    {
        return $this->createQueryBuilder('er')
            ->leftJoin('er.currency', 'c')
            ->addSelect('c')
            ->andWhere('c.isActive = :active')
            ->andWhere('er.effectiveDate = (SELECT MAX(e.effectiveDate) FROM App\Entity\ExchangeRateHistory e)')
            ->orderBy('c.currencyName', 'ASC')
            ->setParameter('active', self::ACTIVE)
            ->getQuery()->getResult();
    }

    public function getLatestEffectiveDate()
    {
        $conn = $this->getEntityManager()->getConnection();
        $query = $conn->executeQuery('SELECT CAST(MAX(effective_date) AS DATE) as max_date FROM exchange_rate_history ');

        return $query->fetch();
    }

    public function getExchangeRateHistoryBy(
        ?\DateTime $fromDate,
        ?\DateTime $toDate,
        array $currencies = []
    ) {
        $query = $this->createQueryBuilder('er')
            ->leftJoin('er.currency', 'c')
            ->addSelect('c')
            ->orderBy('er.effectiveDate, c.currencyName', 'ASC');

        if (!empty($currencies)) {
            $query->andWhere('c.code IN (:currencies)')
                ->setParameter('currencies', $currencies);
        }

        if (!empty($fromDate)) {
            $query->andWhere('er.effectiveDate >= :fromDate')
                ->setParameter('fromDate', $fromDate);
        } else {
            $query->andWhere('er.effectiveDate = (SELECT MAX(e.effectiveDate) FROM App\Entity\ExchangeRateHistory e)');
        }

        if (!empty($toDate)) {
            $query->andWhere('er.effectiveDate <= :toDate')
                ->setParameter('toDate', $toDate);
        }

        return $query->getQuery()->getResult();
    }

//    /**
//     * @return ExchangeRateHistory[] Returns an array of ExchangeRateHistory objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ExchangeRateHistory
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
