<?php

namespace App\DBAL\Types;


use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;

class CurrencyType extends AbstractEnumType
{
    public const THB = 'THB';
    public const USD = 'USD';
    public const HKD = 'HKD';
    public const CAD = 'CAD';
    public const NZD = 'NZD';
    public const SGD = 'SGD';
    public const EUR = 'EUR';
    public const GBP = 'GBP';
    public const CZK = 'CZK';
    public const DKK = 'DKK';
    public const ISK = 'ISK';
    public const IDR = 'IDR';

    protected static $choices = [
        self::THB => 'THB',
        self::USD => 'USD',
        self::CAD => 'CAD',
        self::EUR => 'EUR',
        self::GBP => 'GBP',
        self::SGD => 'SGD',
        self::NZD => 'NZD',
        self::HKD => 'HKD',
        self::CZK => 'CZK',
        self::DKK => 'DKK',
        self::ISK => 'ISK',
        self::IDR => 'IDR',
    ];
}