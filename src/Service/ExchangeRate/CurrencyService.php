<?php

namespace App\Service\ExchangeRate;


use App\DBAL\Types\CurrencyType;
use App\Entity\Currency;
use App\Repository\CurrencyRepository;
use Doctrine\ORM\EntityManagerInterface;

class CurrencyService
{
    /**@var EntityManagerInterface */
    private $em;

    /**@var CurrencyRepository */
    private $repository;

    /**@var ExchangeRateProvider */
    private $exchangeRateProvider;

    /**
     * CurrencyService constructor.
     * @param EntityManagerInterface $em
     * @param CurrencyRepository $repository
     * @param ExchangeRateProvider $exchangeRateProvider
     */
    public function __construct(
        EntityManagerInterface $em,
        CurrencyRepository $repository,
        ExchangeRateProvider $exchangeRateProvider
    ) {
        $this->em = $em;
        $this->repository = $repository;
        $this->exchangeRateProvider = $exchangeRateProvider;
    }

    /**
     * adding new currency/ies only if currency not exists
     */
    public function addCurrencies()
    {
        /** @var ExchangeRateCollection $exchangeRates */
        $exchangeRates = $this->exchangeRateProvider->fetch();

        /** @var $Currency [] $currencies */
        $currencies = $this->repository->findAll();

        foreach ($exchangeRates as $exchangeRate) {
            if (!$this->currencyExists($currencies, $exchangeRate->getCode())) {
                $currency = new Currency();
                $currency->setCode($exchangeRate->getCode())
                    ->setCurrencyName($exchangeRate->getName());
                $this->em->persist($currency);
            }
        }
        $this->em->flush();
    }

    /**
     * @param array $currencies
     * @param string $code
     * @return bool
     */
    private function currencyExists(array $currencies, string $code): bool
    {
        /** @var Currency $currency */
        foreach ($currencies as $currency) {
            if ($currency->getCode() === $code) {
                return true;
            }
        }

        return false;
    }
}