<?php

namespace App\Service\ExchangeRate;


use App\Service\Api\ApiRequestNBP;

class ExchangeRateProvider
{
    /** @var ApiRequestNBP */
    private $nbpRequest;

    /** @var ExchangeRateFormatter */
    private $formatter;

    /**
     * ExchangeRateProvider constructor.
     * @param ApiRequestNBP $nbpRequest
     * @param ExchangeRateFormatter $formatter
     */
    public function __construct(ApiRequestNBP $nbpRequest, ExchangeRateFormatter $formatter)
    {
        $this->nbpRequest = $nbpRequest;
        $this->formatter = $formatter;
    }

    /**
     * @return ExchangeRateCollection
     */
    public function fetch(): ExchangeRateCollection
    {
        $nbpExchangeRate = $this->nbpRequest->fetch();

        return $this->formatter->ratesFormatterNBP(
            $nbpExchangeRate->rates,
            new \DateTime($nbpExchangeRate->effectiveDate));
    }
}