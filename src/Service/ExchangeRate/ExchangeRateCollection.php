<?php

namespace App\Service\ExchangeRate;


use Traversable;

class ExchangeRateCollection implements \IteratorAggregate, \ArrayAccess
{
    /** @var  ExchangeRate[] */
    private $exchangeRates;

    /**
     * ExchangeRateCollection constructor.
     * @param $exchangeRates
     */
    public function __construct($exchangeRates)
    {
        $this->exchangeRates = $exchangeRates;
    }

    public function getIterator()
    {
        return new \ArrayIterator($this->exchangeRates);
    }

    public function offsetExists($offset)
    {
        return array_key_exists($offset, $this->exchangeRates);
    }

    public function offsetGet($offset)
    {
        return $this->exchangeRates[$offset];
    }

    public function offsetSet($offset, $value)
    {
        $this->exchangeRates[$offset] = $value;
    }

    public function offsetUnset($offset)
    {
        unset($this->exchangeRates[$offset]);
    }
}