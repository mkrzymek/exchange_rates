<?php

namespace App\Service\ExchangeRate;

use App\Entity\Currency;
use App\Entity\ExchangeRateHistory;
use App\Repository\CurrencyRepository;
use Doctrine\ORM\EntityManagerInterface;

class ExchangeRateService
{
    /** @var ExchangeRateProvider */
    private $exchangeRateProvider;

    /** @var EntityManagerInterface */
    private $em;

    /** @var CurrencyRepository */
    private $repository;

    /**
     * ExchangeRateService constructor.
     * @param ExchangeRateProvider $exchangeRateProvider
     * @param EntityManagerInterface $em
     * @param CurrencyRepository $repository
     */
    public function __construct(
        ExchangeRateProvider $exchangeRateProvider,
        EntityManagerInterface $em,
        CurrencyRepository $repository
    ) {
        $this->exchangeRateProvider = $exchangeRateProvider;
        $this->em = $em;
        $this->repository = $repository;
    }

    /**
     * add new exchange rates only if current exchange rates are out of date
     */
    public function addExchangeRates(): void
    {
        /** @var ExchangeRateCollection $exchangeRates */
        $exchangeRates = $this->exchangeRateProvider->fetch();

        /** @var Currency[] $currencies */
        $currencies = $this->repository->getActiveCurrenciesWithLatestExchangeRate();
//die();
        foreach ($currencies as $currency) {
            $exchangeRateEffectiveDate = $this->getExchangeRateEffectiveDate($currency->getCode(),
                $exchangeRates);

            if (isset($exchangeRateEffectiveDate) &&
                !$this->ExchangeRateIsUpToDate($currency, $exchangeRateEffectiveDate)
            ) {
                $exchangeRate = new ExchangeRateHistory();
                $exchangeRate->setRate($exchangeRates->offsetGet($currency->getCode())->getMidRate())
                    ->setCurrency($currency)
                    ->setEffectiveDate($exchangeRateEffectiveDate);
                $this->em->persist($exchangeRate);
            }
        }
        $this->em->flush();
    }

    /**
     * @param string $code
     * @param ExchangeRateCollection $exchangeRates
     * @return \DateTime|null
     */
    private function getExchangeRateEffectiveDate(string $code, ExchangeRateCollection $exchangeRates): ?\DateTime
    {
        if ($exchangeRates->offsetExists($code)) {
            return $exchangeRates->offsetGet($code)->getEffectiveDate();
        }

        return null;
    }

    /**
     * @param Currency $currency
     * @param \DateTime $exchangeRateEffectiveDate
     * @return bool
     */
    private function ExchangeRateIsUpToDate(Currency $currency, \DateTime $exchangeRateEffectiveDate)
    {
        return isset($currency->getExchangeRateHistories()[0]) &&
            $exchangeRateEffectiveDate == $currency->getExchangeRateHistories()[0]->getEffectiveDate();
    }
}