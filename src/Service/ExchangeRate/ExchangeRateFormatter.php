<?php

namespace App\Service\ExchangeRate;


use App\DBAL\Types\CurrencyType;

class ExchangeRateFormatter implements ExchangeRateFormatterNBP
{
    /**
     * @param array $rates
     * @param \DateTime $effectiveDate
     * @return ExchangeRateCollection
     */
    public function ratesFormatterNBP(array $rates, \DateTime $effectiveDate): ExchangeRateCollection
    {
        /** @var ExchangeRate[] $formattedRates */
        $formattedRates = [];

        foreach ($rates as $rate) {
            if (CurrencyType::isValueExist($rate->code) && !empty($rate->currency) && !empty($rate->mid)) {
                $formattedRates[$rate->code] = new ExchangeRate(
                    $rate->code,
                    $rate->currency,
                    $rate->mid,
                    $effectiveDate
                );
            }
        }

        return new ExchangeRateCollection($formattedRates);
    }
}