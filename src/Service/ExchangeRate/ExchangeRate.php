<?php
/**
 * Created by PhpStorm.
 * User: michal
 * Date: 10.09.18
 * Time: 17:30
 */

namespace App\Service\ExchangeRate;


class ExchangeRate
{
    /** @var string */
    private $code;

    /** @var string */
    private $name;

    /** @var float */
    private $midRate;

    /** @var \DateTime */
    private $effectiveDate;

    /**
     * ExchangeRate constructor.
     * @param string $code
     * @param string $name
     * @param float $midRate
     * @param \DateTime $effectiveDate
     */
    public function __construct(string $code, string $name, float $midRate, \DateTime $effectiveDate)
    {
        $this->code = $code;
        $this->name = $name;
        $this->midRate = $midRate;
        $this->effectiveDate = $effectiveDate;
    }

    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return float
     */
    public function getMidRate(): float
    {
        return $this->midRate;
    }

    /**
     * @return \DateTime
     */
    public function getEffectiveDate(): \DateTime
    {
        return $this->effectiveDate;
    }
}
