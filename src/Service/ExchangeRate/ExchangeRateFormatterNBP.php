<?php

namespace App\Service\ExchangeRate;


interface ExchangeRateFormatterNBP
{
    public function ratesFormatterNBP(array $rates, \DateTime $effectiveDate);
}