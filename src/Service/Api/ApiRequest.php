<?php

namespace App\Service\Api;


interface ApiRequest
{
    public function fetch();
}