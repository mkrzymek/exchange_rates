<?php

namespace App\Service\Api;


use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\ClientException;
use Symfony\Component\Config\Definition\Exception\Exception;

class ApiRequestNBP implements ApiRequest
{
    const NBP_API_URL = 'http://api.nbp.pl/api/exchangerates/tables/a/today';

    /** @var Client */
    private $client;

    /**
     * ApiRequestNBP constructor.
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * fetch exchange rates for today.
     * NBP updating exchange Rates from monday to friday between 11:45 - 12:15
     *
     * example_response =
     *  [
     *      {
     *          "table": "A",
     *          "no": "176/A/NBP/2018",
     *          "effectiveDate": "2018-09-11",
     *          "rates": [
     *              {
     *                  "currency": "bat (Tajlandia)",
     *                  "code": "THB",
     *                  "mid": 0.113
     *              },
     *              {
     *                  "currency": "dolar amerykański",
     *                  "code": "USD",
     *                  "mid": 3.7087
     *              },
     *              ...
     *          ]
     *      }
     * ]
     *
     * @return Object
     */
    public function fetch(): Object
    {
        try {
            $response = $this->client->request('GET', self::NBP_API_URL);
        } catch (ClientException $e) {
            throw new Exception(Psr7\str($e->getResponse()));
        }

        return json_decode($response->getBody())[0];
    }
}