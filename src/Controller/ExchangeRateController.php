<?php

namespace App\Controller;

use App\Form\ExchangeRateHistoryFormType;
use App\Repository\ExchangeRateHistoryRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ExchangeRateController extends AbstractController
{
    /**
     * @Route("/", name="exchange_rate")
     */
    public function exchangeRate(
        Request $request,
        PaginatorInterface $paginator,
        ExchangeRateHistoryRepository $repository
    ) {
        $exchangeRates = $repository->getLatestActiveExchangeRates();

        $pagination = $paginator->paginate(
            $exchangeRates,
            $request->query->getInt('page', 1),
            10
        );

        return $this->render('exchange_rate/index.html.twig', [
            'pagination' => $pagination,
        ]);
    }

    /**
     * @Route("/history", name="exchange_rate_history")
     */
    public function exchangeRateHistory(
        Request $request,
        PaginatorInterface $paginator,
        ExchangeRateHistoryRepository $repository
    ) {
        $effectiveDate = $repository->getLatestEffectiveDate()['max_date'];
        $form = $this->createForm(ExchangeRateHistoryFormType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $fromDate = $form->getData()['fromDate'];
            $toDate = $form->getData()['toDate'];
            $currencies = in_array('', $form->getData()['currencies']) ? []: $form->getData()['currencies'];

            $exchangeRates = $repository->getExchangeRateHistoryBy(
                $fromDate,
                $toDate,
                $currencies
            );
        }

        $pagination = $paginator->paginate(
            $exchangeRates ?? [],
            $request->query->getInt('page', 1),
            10
        );

        return $this->render('exchange_rate/history.html.twig', [
            'pagination' => $pagination,
            'effectiveDate' => $effectiveDate,
            'form' => $form->createView()
        ]);
    }
}
