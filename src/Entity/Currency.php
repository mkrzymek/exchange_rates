<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Fresh\DoctrineEnumBundle\Validator\Constraints as DoctrineAssert;
use App\DBAL\Types\CurrencyType;


/**
 * @ORM\Entity(repositoryClass="App\Repository\CurrencyRepository")
 */
class Currency
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="CurrencyType", length=3, unique=true)
     * @DoctrineAssert\Enum(entity="App\DBAL\Types\CurrencyType")
     */
    private $code;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $currencyName;

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ExchangeRateHistory", mappedBy="currency")
     */
    private $exchangeRateHistories;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isActive = true;

    public function __construct()
    {
        $this->exchangeRateHistories = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getCurrencyName(): ?string
    {
        return $this->currencyName;
    }

    public function setCurrencyName(string $currencyName): self
    {
        $this->currencyName = $currencyName;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @return Collection|ExchangeRateHistory[]
     */
    public function getExchangeRateHistories(): Collection
    {
        return $this->exchangeRateHistories;
    }

    public function addExchangeRateHistory(ExchangeRateHistory $exchangeRateHistory): self
    {
        if (!$this->exchangeRateHistories->contains($exchangeRateHistory)) {
            $this->exchangeRateHistories[] = $exchangeRateHistory;
            $exchangeRateHistory->setCurrency($this);
        }

        return $this;
    }

    public function removeExchangeRateHistory(ExchangeRateHistory $exchangeRateHistory): self
    {
        if ($this->exchangeRateHistories->contains($exchangeRateHistory)) {
            $this->exchangeRateHistories->removeElement($exchangeRateHistory);
            // set the owning side to null (unless already changed)
            if ($exchangeRateHistory->getCurrency() === $this) {
                $exchangeRateHistory->setCurrency(null);
            }
        }

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }
}
