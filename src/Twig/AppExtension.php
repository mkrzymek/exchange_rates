<?php

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

class AppExtension extends AbstractExtension
{
    public function getFilters(): array
    {
        return [
            // If your filter generates SAFE HTML, you should add a third
            // parameter: ['is_safe' => ['html']]
            // Reference: https://twig.symfony.com/doc/2.x/advanced.html#automatic-escaping
            new TwigFilter('round_to', [$this, 'roundTo']),
        ];
    }

    public function roundTo($value, $digits = 4)
    {
        if ($value == 0) {
            $decimalPlaces = $digits - 1;
        } elseif ($value < 0) {
            $decimalPlaces = $digits - floor(log10($value * -1)) - 1;
        } elseif ($value >= 1) {
            $decimalPlaces = $digits - floor(log10($value));
        } else {
            $decimalPlaces = $digits - floor(log10($value)) - 1;
        }

        $decimalPlaces > $digits - 1 ?: $decimalPlaces = $digits;

        return number_format($value, $decimalPlaces);
    }
}
