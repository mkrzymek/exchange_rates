### Installation:
1. prepare project:
    * `cd exchange_rates/`
    * run `docker-compose up -d`
    * run `docker-compose exec php bash` 
    * run `composer install `
    * run `bin/console doctrine:migrations:migrate`
    * run `bin/console doctrine:fixtures:load`
    
2. update commands for currencies and exchange rates
    * run `bin/console app:update:currencies`
    * run `bin/console app:update:exchange-rates`
